class ArticlesController < ApplicationController
  def new
    @article_new = Article.new
  end

  def create
    redirect_to root_path
  end
end
