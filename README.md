# Semana 13 - Ejemplo 2

### Formularios

1. Introducción a formularios
2. Formulario Básico
3. Procesando formularios
4. Ejemplo formulario de sumas
5. Página dinámica
6. Guardando valores en la base de datos
7. Recuperando una página de la base de datos
8. Redirección después de crear
9. Agregando contenido
10. Formulario por POST
11. Form_tag
12. Form_tag aplicado
13. Form_with
14. Form_with aplicado y byebug
15. Asignación masiva

### CRUD

1. ¿Qué es CRUD?
2. Setup del proyecto Blog
3. CRUD index
4. El método New
5. El método Create
6. El método Show
7. El método Edit
8. El método Update
9. El método Destroy
10. Refactoring (vistas parciales, controllers y rutas)
11. Introducción a validaciones
