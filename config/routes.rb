Rails.application.routes.draw do
  get 'articles/new'
  post 'articles', to: 'articles#create'

  root 'articles#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
